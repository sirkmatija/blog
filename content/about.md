+++
title = "About"
description = "Welmish"
date = "2022-11-22"
aliases = ["about-us", "contact"]
author = "Matija Sirk"
+++

For original content on this website please follow [Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)](https://creativecommons.org/licenses/by-nc-sa/4.0/).
