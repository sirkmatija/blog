+++
title = "HOW-TO: Dual-screen app in React"
description = "Data-dense apps are hard to fit on one screen. Luckily sometimes we can afford to assume our users have two."
summary = "Data-dense apps are hard to fit on one screen. Luckily sometimes we can afford to assume our users have two."
date = "2022-11-22"
author = "Matija Sirk"
tags = [
    "typescript", "React", "broadcast"
]
categories = [
    "how-to"
]
+++

Data-dense apps are hard to fit on one screen. Luckily sometimes we can afford to assume our users have two.

<!--more-->

## Mobile support?

Here you have to forget about it. Provide fallback for getting this data.

## Fallback?

In practice I provided to user "Dual-screen mode" checkbox. If user checked it, secondary data was displayed like in this article. Otherwise it opened in temporary drawer.

## Opening second screen

First we have to figure how to open something in second window.

If opening in new tab is sufficient, it's enough to use [`<a target="_blank" href="your-url-address"/>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/a#attr-target). Based on user's browser configuration provided url will open in new tab (most common default) or in new window. Even if new tab is opened, user can in modern browsers drag it to new window on second monitor.

If it's not we need to use some [`window.open()`](https://developer.mozilla.org/en-US/docs/Web/API/Window/open#Position_and_size_features) ([Reference](https://stackoverflow.com/a/29940606)). For now I only tested first method, so you are on your own.{{% sidenote %}}You probably shouldn't use this anyway. Especially not without user interaction. Popups are impolite, and user will need to drag popup to second monitor anyway, so he doesn't save much time compared to first approach.{{% /sidenote %}}.

```typescript
window.open(
  "your-url-address",
  "Popup",
  "location,status,scrollbars,resizable,width=800, height=800"
);
```

Don't forget to adapt _width_ and _height_ attributes based on expected monitor size. If you decide on this approach responsiveness is pretty much out of the
window any way.

## Synchronizing screens

So cool, we have two windows. But when we click on one of them, second one stays still. We need to synchronize them someway.

Naive option for non-static sites is to use server for this. Any time one of screens makes request, you write it to some table and second screen periodically polls server for changes. But there is better way - you can use clientside.

### Broadcast channel

> The Broadcast Channel API's self-contained interface allows cross-context communication. It can be
> used to detect user actions in other tabs within a same origin, like when the user logs in or out.
>
> — <cite>MDN: [Broadcast Channel API](https://developer.mozilla.org/en-US/docs/Web/API/Broadcast_Channel_API)</cite>

Sounds just like what we need. Let's wrap it into hook:

```typescript
import { useCallback, useEffect, useMemo, useState } from "react";

function useBroadcast<T>(channelName: string, initialState: T) {
  const [state, setState] = useState(initialState);

  const channel = useMemo(
    () => new BroadcastChannel(channelName),
    [channelName]
  );

  const messageHandler = useCallback(function (e: MessageEvent<T>) {
    e.preventDefault();

    const { data } = e;

    setState(data);
  }, []);

  const setter = useCallback(
    function (value: T) {
      channel.postMessage(value);

      setState(value);
    },
    [channel]
  );

  useEffect(() => {
    channel.onmessage = messageHandler;

    return () => channel.close();
  }, [channel, messageHandler]);

  return [state, setter] as [T, (newState: T) => void];
}

export default useBroadcast;
```

So this hook provides us some state that is synchronized between tabs (or windows).
