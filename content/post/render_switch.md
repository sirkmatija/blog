+++
title = "HOW-TO: Render switch statement"
description = "Nested ternary operators quickly become unreadable. But usually we cannot use statements inside React component return statement. Unless we use IIFE."
summary = "Nested ternary operators quickly become unreadable. But usually we cannot use statements inside React component return statement. Unless we use IIFE."
date = "2022-11-24"
author = "Matija Sirk"
tags = [
    "typescript", "React", "switch", "IIFE"
]
categories = [
    "how-to"
]
+++

Nested ternary operators quickly become unreadable. But usually we cannot use statements inside React component return statement. Unless we use [IIFE](https://developer.mozilla.org/en-US/docs/Glossary/IIFE).

<!--more-->

```typescript
enum StateFlag {
  OK,
  LOADING,
  ERROR,
}

function Switched(props: { state: StateFlag }) {
  return (
    <div>
      {(() => {
        switch (props.state) {
          case StateFlag.OK:
            return <p>Ok.</p>;
          case StateFlag.LOADING:
            return <p>Loading...</p>;
          case StateFlag.ERROR:
            return <p>Error!</p>;
        }
      })()}
    </div>
  );
}
```

Break after return is not needed, since return prevents fall-through behaviour anyway.
