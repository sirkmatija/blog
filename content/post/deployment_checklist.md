+++
title = "CHECKLIST: Django Rest Framework + React SPA Deployment"
description = "Your app works on localhost. Now what?"
summary = "Your app works on localhost. You have a Windows server with Apache. What now?"
date = "2022-11-24"
author = "Matija Sirk"
tags = [
    "React", "Django Rest Framework", "Deployment", "Apache"
]
categories = [
    "checklist"
]
+++

Your app works on localhost. You have a Windows server with Apache. What now?

<!--more-->

Let's assume frontend will be hosted on your-domain.example/_app-name_ and api on your-domain.example/_api-name_.

1. Adapt [Django settings for production](https://docs.djangoproject.com/en/4.1/howto/deployment/checklist/).

   - For simple projects single `settings.py` file with `IS_PROD = True` and conditionals in crucial
     settings is probably enough.
   - For more complex projects adapt `settings/base.py`, `settings/development.py`,
     `settings/production.py` structure.

2. Create a simple `api.bat` file that will run server on port (for example 5004).

```bat
.\.venv\Scripts\activate && python manage.py runserver localhost:5004
```

3. In frontend adapt files in `public` folders (`title` in `index.html`, `manifest.json`, favicons, ...). If you are using `react-router` create `public\.htaccess`. Be careful that
   syntax depends on Apache version and this one may be old:

```
<IfModule mod_rewrite.c>
  RewriteEngine On
  RewriteBase /app-name
  RewriteRule ^index\.html$ - [L]
  RewriteCond %{REQUEST_FILENAME} !-f
  RewriteCond %{REQUEST_FILENAME} !-d
  RewriteCond %{REQUEST_FILENAME} !-l
  RewriteRule . /app-name/index.html [L]
  RewriteCond %{HTTPS} off
  RewriteRule (.*) https://%{HTTP_HOST}%{REQUEST_URI} [R=301,L]
</IfModule>
```

4. Add `"homepage": "http://localhost/app-name/"` to `package.json`.

5. Add `basename` to router:

```typescript
export const router = createBrowserRouter(
  [
    // ... your paths.
  ],
  { basename: "/app-name" }
);
```

6. Add your api to Apache configuration to `extra/httpd-vhosts.conf` (adapt port to your choice in 2.):

```
<VirtualHost *:80>
    ... your VirtualHost configuration.

    ProxyPass /api-name http://localhost:5004
    ProxyPassReverse /api-name http://localhost:5004
</VirtualHost>

<VirtualHost *:443>
    ... your VirtualHost configuration.

    ProxyPass /api-name http://localhost:5004
    ProxyPassReverse /api-name http://localhost:5004
</VirtualHost>

```

7. In frontend adapt api address based on production/development, for example:

```typescript
export const LOCALMODE = process.env.NODE_ENV !== "production";

export const API_SERVER = LOCALMODE
  ? "http://localhost:8000/api"
  : "https://your-domain.example/api-name";
```

8. Add your `api.bat` to Task manager trigger: _start-up_.

9. Run `npm run build` and copy `build` folder from frontend to `htdocs` (or whatever directory is exposed to web).

10. Prepare initial database and copy it to production.

11. Verify api is using production settings.

12. Good luck!
